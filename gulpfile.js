'use strict';

const gulp = require('gulp');
const watch = require('gulp-watch');
const autoprefixer = require('gulp-autoprefixer');
const uglify = require('gulp-uglify');
const sass = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const rigger = require('gulp-rigger');
const cleanCSS = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const rimraf = require('rimraf');
const babel = require('gulp-babel');
const gulpIf = require('gulp-if');
const debug = require('gulp-debug');
const newer = require('gulp-newer');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const browserSync = require("browser-sync");
const reload = browserSync.reload;

const isDevelopment = !process.env.NODE_ENV || process.env.NODE_ENV == 'development';

const path = {

  build: { //Тут мы укажем куда складывать готовые после сборки файлы
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    img: 'build/img/',
    fonts: 'build/fonts/'
  },
  src: { //Пути откуда брать исходники
    html: 'src/*.html', //Синтаксис src/*.html говорит gulp что мы хотим взять все файлы с расширением .html
    js: 'src/js/main.js',//В стилях и скриптах нам понадобятся только main файлы
    style: 'src/scss/main.scss',
    img: 'src/img/**/*.*', //Синтаксис img/**/*.* означает - взять все файлы всех расширений из папки и из вложенных каталогов
    fonts: 'src/fonts/**/*.*'
  },
  watch: { //Тут мы укажем, за изменением каких файлов мы хотим наблюдать
    html: 'src/**/*.html',
    js: 'src/js/**/*.js',
    style: 'src/scss/**/*.scss',
    img: 'src/img/**/*.*',
    fonts: 'src/fonts/**/*.*'
  },
  clean: './build'
};

const config = {
  server: {
    baseDir: "./build"
  },
  tunnel: true,
  host: 'localhost',
  port: 9000,
  logPrefix: "Frontend_Devil"
};

gulp.task('html:build', function() {
  gulp.src(path.src.html) //Выберем файлы по нужному пути
      .pipe(rigger()) //Прогоним через rigger
      .pipe(gulp.dest(path.build.html)) //Выплюнем их в папку build
      .pipe(browserSync.stream()); //И перезагрузим наш сервер для обновлений
});

gulp.task('js:build', function() {
  gulp.src(path.src.js) //Найдем наш main файл
      .pipe(plumber({
        errorHandler: notify.onError(function(err) {
          return {
            title: 'Styles',
            message: err.message
          };
        })
      })) // отлов ошибок
      .pipe(newer(path.build.js + 'main.js'))//не даеёт пересобирать файлы если они есть и не менялись
      .pipe(rigger()) //Прогоним через rigger
      .pipe(babel({
        presets: ['env']
      }))
      .pipe(gulpIf(isDevelopment, sourcemaps.init())) //Инициализируем sourcemap
      .pipe(uglify()) //Сожмем наш js
      .pipe(gulpIf(isDevelopment, sourcemaps.write())) //Пропишем карты
      .pipe(gulp.dest(path.build.js)) //Выплюнем готовый файл в build
      .pipe(reload({ stream: true })); //И перезагрузим сервер

});

gulp.task('style:build', function() {
  gulp.src(path.src.style) //Выберем наш main.scss
      .pipe(plumber({
        errorHandler: notify.onError(function(err) {
          return {
            title: 'Scripts',
            message: err.message
          };
        })
      }))
      // .pipe(newer(path.build.css + 'main.css'))//не даеёт пересобирать файлы если они есть и не менялись
      .pipe(gulpIf(isDevelopment, sourcemaps.init()))//То же самое что и с js
      .pipe(sass()) //Скомпилируем

      .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      })) //Добавим вендорные префиксы
      .pipe(cleanCSS({ debug: true }, (details) => {
        console.log(`${details.name}: ${details.stats.originalSize}`);
        console.log(`${details.name}: ${details.stats.minifiedSize}`);
      })) //Сожмем , format: 'keep-breaks'/'beautify
      .pipe(gulpIf(isDevelopment, sourcemaps.write()))
      .pipe(gulp.dest(path.build.css)) //И в build
      .pipe(reload({ stream: true }));
});

gulp.task('image:build', function() {
  gulp.src(path.src.img) //Выберем наши картинки
      .pipe(newer(path.build.img)) //не даеёт пересобирать файлы если они есть и не менялись
      .pipe(imagemin({ //Сожмем их
        progressive: true,
        svgoPlugins: [{ removeViewBox: false }],
        use: [pngquant()],
        interlaced: true
      }))
      .pipe(gulp.dest(path.build.img)) //И бросим в build
      .pipe(reload({ stream: true }));
});

gulp.task('fonts:build', function() {
  gulp.src(path.src.fonts)
      .pipe(newer(path.build.fonts))//не даеёт пересобирать файлы если они есть и не менялись
      .pipe(gulp.dest(path.build.fonts))
});

gulp.task('build', [
  'html:build',
  'js:build',
  'style:build',
  'fonts:build',
  'image:build'
]);

gulp.task('watch', function() {
  watch([path.watch.html], function(event, cb) {
    gulp.start('html:build');
  });
  watch([path.watch.style], function(event, cb) {
    gulp.start('style:build');
  });
  watch([path.watch.js], function(event, cb) {
    gulp.start('js:build');
  });
  watch([path.watch.img], function(event, cb) {
    gulp.start('image:build');
  });
  watch([path.watch.fonts], function(event, cb) {
    gulp.start('fonts:build');
  });
});

gulp.task('webserver', function() {
  browserSync(config);
});

gulp.task('clean', function(cb) {
  rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'webserver', 'watch']);